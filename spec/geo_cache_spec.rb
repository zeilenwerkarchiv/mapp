require_relative '../lib/geo_cache'

describe GeoCache do
  before :each do

  end

  it 'stores once cached locations' do
    GeoCache.geocode('Ittigen')
    expect(GeoCache.cache).to include('Ittigen')
  end

  it 'retrieves cached locations' do
    loc1 = GeoCache.geocode('Ittigen')
    loc2 = GeoCache.geocode('Ittigen')
    expect(loc1).to eq(loc2)
  end
end
