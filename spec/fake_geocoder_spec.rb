require_relative '../lib/fake_geocoder'

describe Geokit::Geocoders::FakeGeocoder do
  it 'returns location' do
    loc = Geokit::Geocoders::FakeGeocoder.geocode('Ittigen')
    expect(loc.lat).to be_within(180).of(0)
    expect(loc.lng).to be_within(90).of(0)
  end
end
