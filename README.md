# mApp

Visualisierung von Twitter-Bekanntschaften mit Hilfe von d3.

Demo auf [opendata-unibe.herokuapp.com](http://opendata-unibe.herokuapp.com).

## Vorbereitungen

* Ruby >= 2.0.0 mit RubyGems installieren
* Bundler mit `gem install bundler --no-rdoc --no-ri` installieren
* Folgende Umgebungsvariablen in `.env`-Datei definieren oder in `config.rb` schreiben:

```shell
API_KEY=AAAA
API_SECRET=BBBB
OWNER=CCCC
OWNER_ID=DDDD
ACCESS_TOKEN=EEEE
ACCESS_TOKEN_SECRET=FFFF
```
Details unter [bkeepers/dotenv](https://github.com/bkeepers/dotenv).

Diese Variablen können auf der Twitter-Entwicklerseite generiert werden.

## Anleitung

* Repo klonen: `git clone git@bitbucket.org:Zeilenwerk/mapp.git`
* Gems installieren: `bundle install`
* Server starten: `ruby app.rb` oder `shotgun app.rb` für instant-reload.

&copy; 2014 [Zeilenwerk](http://www.zeilenwerk.ch)
