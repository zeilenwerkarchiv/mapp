require 'sinatra'
require 'twitter'
require 'delegate'
require 'data_mapper'
require 'rack-google-analytics'

DataMapper.setup(:default, ENV['DATABASE_URL'] || 'postgres://localhost/opendata_unibe')

require_relative 'boilerplate'
require_relative 'config'

set :erb, :layout => :'layout'

use Rack::GoogleAnalytics, tracker: 'UA-45945746-6'

get '/' do
  erb :home
end

get '/show' do
  @user = params[:user] || "LukasSkywalker"
  @user.gsub!('@', '')
  
  fetch_data
  
  erb :index
end

def fetch_data
  @friends = get_friends.map{ |u| format_user(u)}.to_json
  @myself = format_user(get_self).to_json
rescue Twitter::Error::TooManyRequests => e
  puts 'WARNING: twitter: TooManyRequests'
  Client.next
  fetch_data
end

def get_self
  u = Client.instance.users(@user).first
  User.new(u).fetch_location
end

def get_friends
  puts 'INFO: getting twitter data'
  friend_ids = []

  Client.instance.friend_ids(@user).each { |id|
    friend_ids << id 
  }

  friends = Client.instance.users(friend_ids)

  puts 'INFO: twitter data fetched'
  puts 'INFO: getting location data'

  GeoCache.start

  ret = friends.map { |user|
    User.new(user).fetch_location
  }.compact
  
  puts 'INFO: location data fetched'
  
  puts "INFO: Cache Hit Rate: #{GeoCache.hit.to_f / (GeoCache.hit + GeoCache.miss) * 100}"
  
  ret
end

def format_user(user)
  return {name: user.name, lat: user.lat, lng: user.lng, img: user.profile_image_url, screen_name: user.screen_name, loc: user.location}
end

class User
  attr_accessor :lat, :lng
  def initialize(obj)
    @obj = obj
  end
  def method_missing(method, *args)
    @obj.send(method, *args)
  end
  def fetch_location
    if self.location.empty?
      self.lat = 0
      self.lng = 0
    else
      a = GeoCache.geocode(self.location)
      self.lat = a.lat
      self.lng = a.lng
    end
    self
  end
end

class Client
  TWITTER_KEYS = [
    {key: 'm8YX9mfauwVMXsf929kCw2SAl', secret: 'atPdFkNhSb1aLVyw1xJamIqtpeGcjAnUxSdXzKBeacrPlFoBBL'},
    {key: 'fqlbhX1OsoAF43PKhbdq0t3nw', secret: 'nI9xsB2XuflXYXmesnJFDIlAZk1MpNXF6yChDkiwXVO2g54XtI'},
    {key: 'LeUog31p5R7AjJAZFAHESobQ0', secret: 'rbQysmzPSid8cGsjKkQtDMEWCWTWttCRtFb4XwUjaoSN8FmQSu'},
    {key: 'YENXTvPgBfgN3rgdqqEurT2kr', secret: 'yZPHQKmwXXd1b7mGN4Tc1a9RCd7fOQAUnmXwzaSgmGZONcpcae'}
  ]
  
  def self.instance
    Twitter::REST::Client.new do |config|
      config.consumer_key    = Client.current_key[:key]
      config.consumer_secret = Client.current_key[:secret]
    end
  end
  
  def self.next
    @@current_index ||= 0
    if @@current_index == TWITTER_KEYS.count
      @@current_index = 0 
    else
      @@current_index += 1
    end
    puts 'INFO: twitter: trying next key, index is now ' + @@current_index.to_s
    Client.instance
  end
  
  def self.current_key
    @@current_index ||= 0
    TWITTER_KEYS[@@current_index]
  end
end
