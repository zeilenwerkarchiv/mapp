require 'geokit'

module Geokit
  module Geocoders
    # and use :my to specify this geocoder in your list of geocoders.
    class FakeGeocoder < Geocoder
      # Use via: Geokit::Geocoders::FakeGeocoder.key = 'MY KEY'
      config :key

      private

      def self.do_geocode(address, options = {})
        self.parse_json(nil)
      end

      def self.parse_json(json)
        loc = new_loc
        loc.lat = -90 + rand() * 180
        loc.lng = -180 + rand() * 360
        loc.success = true
        loc
      end
    end
  end
end
