require 'ostruct'
require 'json'
require 'rest_client'

KEYS = ['AIzaSyDvRds7G6q1CQ6WOypxWuIGyuXXUlgZ1oo', 'AIzaSyBMfFebZX-rsk3UygSeST_ZhklMVg9I9UI', 'AIzaSyAjPl4yzI4xYU9yw2GgptYV547pFo3ounE', 'AIzaSyB7hKm_V3w43wgaJiI6HBsou0BMtimSBX8', 'AIzaSyD59rBnMgHdYzHzBvN48B9H1nGVM9WbHsI']

class GCoder
  URL = "https://maps.googleapis.com/maps/api/geocode/json"
  @@key_index = 0
  
  def self.get(loc)
    response = RestClient.get URL, {params: {sensor: false, address: loc, key: GCoder.current_key}}
    begin
      lc = JSON.parse(response.to_str)['results'].first['geometry']['location']
    rescue StandardError
      puts 'WARNING: location fetch: StandardError'
      GCoder.next_key
      lc = {'lat' => 0, 'lng' => 0}
    end
    OpenStruct.new(lat: lc['lat'], lng: lc['lng'])
  end
  
  def self.next_key
    if KEYS[@@key_index] == KEYS.last
      @@key_index = 0
    else
      @@key_index += 1
    end
    puts 'INFO: trying gcoder key at index ' + @@key_index.to_s
  end
  
  def self.current_key
    KEYS[@@key_index]
  end
end
