class Location
  include DataMapper::Resource

  property :id,         Serial
  property :location,   String
  property :lat,        Float
  property :lng,        Float
end

DataMapper.finalize

DataMapper.auto_upgrade!
