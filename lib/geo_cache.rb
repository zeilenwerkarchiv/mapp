require 'geokit'
require 'ostruct'
require_relative 'fake_geocoder'
require_relative 'gcoder'

Geokit::Geocoders::BingGeocoder.key = 'AlFZOAhh-mRBhXozuSuV4wLh8BM-Z9dhbi1u4KLxSo3GMara9HzVQkN-dPREfRNA'
Geokit::Geocoders::GeonamesGeocoder.key = 'quevita'

Geokit::Geocoders::provider_order = [:google, :geonames, :bing, :fake]

class GeoCache
  # Geocodes a location string to its corresponding latitude and longitude
  # Usage 
  # location = GeoCache.geocode('Ittigen')
  # location.lat #=> 47.32435
  # location.lng #=>  7.23456
  # GeoCacher caches fetched results to stay below the rate limits
  
  def self.start
    @@hit = 0
    @@miss = 0
  end
  
  def self.hit
    @@hit
  end
  
  def self.miss
    @@miss
  end

  def self.geocode(loc)
    if Location.count(location: loc.downcase) > 0
      @@hit += 1
      ll = Location.first(location: loc.downcase)
      lat = ll.lat
      lng = ll.lng
    else
      @@miss += 1
      ll = GCoder.get(loc)
      lat = ll.lat
      lng = ll.lng
      #Location.create(location: loc.downcase, lat: lat, lng: lng)
    end
    OpenStruct.new(location: loc, lat: lat, lng: lng)
  end
end
