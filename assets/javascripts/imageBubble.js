imageBubblePlugin = function ( layer, data, options ) {  
	// hold this in a closure
    var self = this;

    // a class you'll add to the DOM elements
    var className = 'imageBubble';

    // make a D3 selection.
    var bubbles = layer
           .selectAll(className)
           .data( data, JSON.stringify );

    bubbles
      .enter()
	    .append("svg:image")
	      .attr("xlink:href", function(datum) { return datum.img} )
		  .attr('x', function ( datum ) {
		    return self.latLngToXY(datum.destination.latitude, datum.destination.longitude)[0] - 24;
		  })
		  .attr('y', function ( datum ) {
		    return self.latLngToXY(datum.destination.latitude, datum.destination.longitude)[1] - 24;
		  })
	      .attr("width", 48)
	      .attr("height", 48)
		  .on("click", function(d) { window.location.href = '/show?user=' + d.screen_name });
}
