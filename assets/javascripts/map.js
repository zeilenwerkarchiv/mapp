$(function() {
    var map = new Datamap({
        element: document.getElementById('container'),
		geographyConfig: {
			borderColor: '#222',
			highlightOnHover: false,
			popupOnHover: false,
		},
		fills: {
		defaultFill: '#88F'
		}
    });
    var objLit = [];
    for(var i = 0; i < friends.length; i++) {
    	objLit.push({ origin: { latitude: mySelf.lat, longitude: mySelf.lng }, destination: { latitude: friends[i].lat, longitude: friends[i].lng }, img: friends[i].img, screen_name: friends[i].screen_name, loc: friends[i].loc, options:{strokeColor: 'rgba(255, 69, 0, 0.7)', strokeWidth: 2} });
    }
    map.arc(objLit);
    map.addPlugin('imageBubble', imageBubblePlugin);
    map.imageBubble(objLit, {makeTheBubbleThisColor: 'blue'});
	
	
	$('image').hover(function(e) {
		// mouse in
	    var img = $(this).attr('href');
	   	var p;
	   	for(var i=0; i<friends.length; i++) {
			if(friends[i]['img'] == img) {
				p = friends[i];
			}
	   	}
		var popup = $('<div class="ui-tooltip ui-widget ui-corner-all ui-widget-content"></div>');
	   	var content = '<img src="' + p.img.replace('_normal', '') + '"/>' + '<h3>' + p.name + ' (@' + p.screen_name + ')</h3>' +
		'<h5>' + p.loc + '</h5>';
		popup.append(content);
		popup.css({top: e.pageY + 20, left: e.pageX + 20});
		popup.hide();
		$('body').append(popup);
		$(popup).fadeIn(800);
		console.log('asdfg');
	}, function(e){
		$('.ui-tooltip').fadeOut(800, function (){
			$(this).remove()
		});
	});
});
