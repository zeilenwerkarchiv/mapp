var Popup = {
    element: function() { return $('.popup'); },
    show: function() { this.element().show(); },
    hide: function() { this.element().hide(); },
    setImage: function(src) { this.element().find('img').attr('src', src); },
    setTitle: function(title) { this.element().find('h3').text(title); }
};
