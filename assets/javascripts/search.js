$(function() {
	$('#searchbutton').click(function(){
    	trySearch();
	});
    
    $('#searchfield input').keyup(function(e){ 
        var code = e.which;
        if(code==13) e.preventDefault();
        if(code==32||code==13||code==188||code==186) {
            trySearch();
        }
    });
});

function trySearch() {
	var a =	$('#searchfield input').val()
	if (a == ""){
		alert ("Bitte einen Namen eingeben")}
	else { 
        $('#loader').show();
		window.location.href = "/show?user=" + a;
	}
}
