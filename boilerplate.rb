Dir.glob('lib/*.rb').each do |file|
  require_relative file
end

get '/images/:name' do
  name = params[:name]
  send_file 'assets/images/' + name
end

get "/scripts/:name" do
  name = params[:name]
  send_file 'assets/javascripts/' + name
end

get "/styles/:name" do
  name = params[:name]
  send_file 'assets/stylesheets/' + name
end
